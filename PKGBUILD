# Maintainer: David Runge <dvzrv@archlinux.org>
# Contributor: speps <speps at aur dot archlinux dot org>
# Contributor: Orivej Desh <masecretaire@gmx.fr>

pkgname=sonic-visualiser
pkgver=5.2
pkgrel=1
pkgdesc="A viewer and analyser of music audio files."
arch=(x86_64)
url="https://www.sonicvisualiser.org/"
license=(GPL-2.0-or-later)
groups=(pro-audio)
depends=(
  gcc-libs
  glibc
  hicolor-icon-theme
  libmad
  opusfile
  qt6-base
  qt6-svg
  vamp-plugin-sdk
)
makedepends=(
  alsa-lib
  bzip2
  capnproto
  fftw
  libfishsound
  libid3tag
  liblo
  liblrdf
  liboggz
  libpulse
  libsamplerate
  libsndfile
  meson
  portaudio
  rubberband
  serd
  sord
)
provides=(vamp-host)
source=(https://github.com/$pkgname/$pkgname/releases/download/sv_v$pkgver/$pkgname-$pkgver.tar.gz)
sha512sums=('31296b0876d4aa5082526244f1dfd1debf9feaf4ca1dfa4dc3eeca68ce43e7292b2cff2f2edb31aebc60c2f10f97ff5cd0dc4e88ca440f1187e7ee5e4714036b')
b2sums=('0a3bafa362b9b6284defbfb41f9f72e984182075aea962a1072fb7148cac3137dfd881d2e65be0d51a11c5863bb9a842355d949c74c101f9526bacf7769474c6')

build() {
  arch-meson build $pkgname-$pkgver
  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs
}

package() {
  depends+=(
    alsa-lib libasound.so
    bzip2 libbz2.so
    capnproto libcapnp.so libkj.so
    fftw libfftw3.so
    libfishsound libfishsound.so
    libid3tag libid3tag.so
    liblo liblo.so
    liblrdf liblrdf.so
    liboggz liboggz.so
    libpulse libpulse.so
    libsamplerate libsamplerate.so
    libsndfile libsndfile.so
    portaudio libportaudio.so
    rubberband librubberband.so
    serd libserd-0.so
    sord libsord-0.so
  )

  DESTDIR="$pkgdir" meson install -C build
  install -vDm 644 $pkgname-$pkgver/samples/* -t "$pkgdir/usr/share/$pkgname/samples"
  install -vDm 644 $pkgname-$pkgver/templates/* -t "$pkgdir/usr/share/$pkgname/templates"
  install -vDm 644 $pkgname-$pkgver/{CHANGELOG,CITATION,README{,_OSC}.md} -t "$pkgdir/usr/share/doc/$pkgname"
}
